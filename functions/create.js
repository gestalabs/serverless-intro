//
// Función Lambda que crea una nueva nota en la tabla 'notes' de DynamoDB.
//

import { v1 as uuidv1 } from 'uuid';
import handler from '../libs/handler';
import dynamoDB from '../libs/dynamoDB';

export const main = handler(async (event, context) => {

    // Obtenemos el 'body' de event, es decir, la nueva nota que se quiere crear.
    const data = JSON.parse(event.body);

    // Definimos los parametros necesarios para crear una nueva nota.
    const params = {
        // Nombre de la tabla donde se guardará.
        TableName: process.env.tableName,

        // Item. Nota con sus campos.
        Item: {
            userId: event.requestContext.identity.cognitoIdentityId,
            noteId: uuidv1(),
            content: data.content,
            attachment: data.attachment,
            createdAt: Date.now()
        }
    };

    // Mandamos a llamar la función 'put' de nuestra librería dynamoDB.
    await dynamoDB.put(params);
});