//
// Librería DynamoDB
//

import AWS from 'aws-sdk';

const dynamoClient = new AWS.DynamoDB.DocumentClient();

export default {
    put: (params) => dynamoClient.put(params).promise(),
};
