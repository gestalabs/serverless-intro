# Introducción Serverless 

Para ejecutar el código satisfactoriamente, deben cumplirse los [prerrequisitos](https://docs.google.com/document/d/1KsWEWfJ_GHALMrxeAqYHnO9_-9M0loL6EocaD_cy6rw/edit?usp=sharing)

## Instalación

Con Yarn:
``` bash
$ yarn install 
```
o con NPM:
``` bash
$ npm install 
```

## Demo

Para ejecutar el demo y crear una nueva nota en la tabla notes de DynamoDB

``` bash
$ serverless invoke local --function create --path mocks/create-event.json
```
